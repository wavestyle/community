# Release SIG

The Release SIG aims at development and release management, including plan, requirement, issue, risk and so on.

## SIG Mission and Scope

- requirement and feature management

- release plan, development plan

- development process and risk managemnet

- release scope management, release note

## Repository
- release-management
- all

## SIG members
### Owner
- Jack Yu
- Fighting Liu
- handsome_feng

### Maintainers
- maozhou
- handsome_feng
- xiewei
- Allen
- Shang Xiaoyang


## Email
release@lists.openkylin.top
