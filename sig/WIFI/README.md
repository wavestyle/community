# WIFI兴趣小组（SIG）

WIFI SIG组负责WIFI IC在openKylin PC、信创、服务器、边缘计算等生态设备上的WIFI 驱动适配开发、版本维护、源码仓库管理和开发手册编写等工作，致力于打造适配openKylin系统WIFI 驱动。


## 工作目标

- 适配openKylin系统PC、信创、服务器、边缘计算等设备上的WIFI 驱动;


## SIG成员

### Owner

- lfHuangwz

### Maintainers
- lfHuangwz

## SIG维护包列表
- Realtek-WIFI

## SIG邮件列表
wifi@lists.openkylin.top
