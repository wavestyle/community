AI-EdgeCloud（SIG）
AI-EdgeCloud SIG小组团队致力于将公司边缘智慧云产品适配openKylin操作系统，形成智能边缘云openKylin试用版本，形成针对常规、云、容器、智能等应用场景的通用操作系统方案，共建openKylin生态。

工作目标
负责将智慧边缘云产品适配openKylin操作系统，并进行优化。

repository
edgecloud-project

SIG成员
Maintainers
fantianqing(fantianqing@eicas.com.cn)
wangzhilin(wangzhilin@eicas.com.cn)
weiyanxun(weiyanxun@eicas.com.cn)
longyu(longyu@eicas.com.cn)
wangruiyong(wangruiyong@eicas.com.cn)
wanghui(wanghui@eicas.com.cn)
jiangting(jiangting@eicas.com.cn)