##  系统服务SIG（SystemService SIG）

系统服务小组致力于服务管理的技术研究，提供系统基础组件的集合和服务管理，提供稳定的系统底层环境。

### 工作目标

1. 提供稳定的服务管理基础功能包括服务管理、登录、udev、resolve、cgroup；
2. 提供账号管理基础功能。

## SIG成员

### Owner

- 史龙真  shilongzhen@kylinos.cn
- 廖先福  liaoxianfu@kylinos.cn

### Maintainers

- 王帅(wangshuai2@kylinos.cn)
- 何骁骁(hexiaoxiao@kylinos.cn)
- 张朋(zhangpeng1@kylinos.cn)

## SIG邮件列表

- systemservice@list.openkylin.top

## SIG维护包列表

- systemd
- accountsservice
